#!/usr/bin/env python

## python stat1.py genome_test.fa out.txt

import sys

fa_file = open(sys.argv[1], 'r')

# generate fa dictionary: {'chr1':'atacg', 'chr2':'atatata'}
fa_dict = {}
fa = fa_file.readlines()
for line in fa:
	line = line.strip()
	if line[0] == '>':
		header = line[1:]
	else:
		fa_dict[header] = fa_dict.get(header, '') + line

# calculate total length, maximum chromesome, and maximum chromesome length
total_length = 0
max_length = 0
max_chr = ''
for chr in fa_dict.keys():
	total_length = total_length + len(fa_dict.get(chr))
	if len(fa_dict.get(chr)) > max_length:
		max_length = len(fa_dict.get(chr))
		max_chr =  chr
	else:
		continue

# calculate GC content
seq_list = [seq for seq in fa_dict.values()]
total_seq = ''.join(seq_list)
GC_content = (total_seq.count('G') + total_seq.count('C')) / (total_seq.count('G') + total_seq.count('C') + total_seq.count('T') + total_seq.count('A')) * 100

# write output file
out_file = open(sys.argv[2], 'w')
out_file.write('total length: ' + str(total_length) + '\n')
out_file.write('max chr: ' + max_chr + '\n')
out_file.write('max chr length: ' + str(max_length) + '\n')
out_file.write('GC content: ' + str(round(GC_content, 2)) + '%' + '\n')

fa_file.close()
out_file.close()
