# RNA-Seq workflow



>*Author: licheng*
>
>*E-mail: licheng20080921@126.com*
>
>*Usage: RNA-Seq*
>
>*Create date: 2018/12/08*



先安装清华的[anaconda](https://mirrors.tuna.tsinghua.edu.cn/anaconda/archive/)或者[miniconda](https://mirrors.tuna.tsinghua.edu.cn/anaconda/miniconda/), 按照[说明书](https://mirrors.tuna.tsinghua.edu.cn/help/anaconda/)进行安装和配置。

更改镜像配置下载安装软件之前先搜索[软件是否存在](https://anaconda.org/bioconda/)。

更改镜像源配置如下：

```shell
## conda下载，安装
wget https://mirrors.tuna.tsinghua.edu.cn/anaconda/archive/Anaconda3-5.3.1-Windows-x86_64.sh
bash Anaconda3-5.3.1-Windows-x86_64.sh

## 配置conda
conda config --add channels https://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/free
conda config --add channels https://mirrors.tuna.tsinghua.edu.cn/anaconda/cloud/conda-forge
conda config --add channels https://mirrors.tuna.tsinghua.edu.cn/anaconda/cloud/bioconda
conda config --set show_channel_urls yes
```



---



## 1. 创建RNA-Seq环境 

```shell
conda create -n rna-seq
conda info --envs
conda activate rna-seq
conda deactivate
```



##### conda环境的备份

```shell
# 输出备份
conda env export -n rna-seq > rna-seq.yaml

# 倒入备份
conda create -n rna-seq -f rna-seq.yaml
```





---



## 2. 软件安装

需要安装的软件，见 ”幕布“ RNA-Seq。

```shell
conda sereach hisat2 trim-galore subread
conda install -y hisat2 trim-galore subread
```



---



## 3. 数据库下载数据并格式转换

```shell
conda activate rna
# 下载数据
cat id | while read id; do (prefetch $id &); done
# sra2fastq
ls ~/my_project/sra/* | while read id; do (nohup fastq-dump --gzip --split-3 -O ./ $id &); done
```



---



## 4. 原始数据质控

当前路径："02.QC"

```shell
ls ../01.raw_data/*.fastq.gz | xargs -i echo nohup fastqc -o ./ -t 10 --nogroup {} \& > fastqc.sh

bash fastqc.sh

multiqc ./
```



---



## 5. 过滤

### 5.1. trim_galore

当前路径："03.clean_data"

生成配置文件：config_trim_galore

```shell
ls ../01.raw_data/*_1.fastq.gz > 1
ls ../01.raw_data/*_2.fastq.gz > 2
paste 1 2 > config_trim_galore
```

 2shell 脚本：

```shell
trim_galore=trim_galore
dir='home/u3065/my_project/03.clean_data/trim_galore/'
cat $1 | while read id
do
	arr=($id)
	fq1=${arr[0]}
	fq2=${arr[1]}
	
	nohup trim_galore -q 25 --phred33 --length 50 -e 0.1 --gzip --stringency 3 --paired -o $dir $fq1 $fq2 &
done

bash trim_galore.sh config_trim_galore
```



### 5.2. trimmomatic

当前路径："03.clean_data"

生成配置文件: config_trimmomatic （利用 sed 命令，结合 paste 命令）

```shell
paste ../*_1.fastq.gz ../*_2.fastq.gz 1_trimmomatic_paired.fastq.gz 1_trimmomatic_unpaired.fastq.gz 2_trimmomatic_paired.fastq.gz 1_trimmomatic_unpaired.fastq.gz > config_trimmomatic
```

当前路径："03.clean_data/trimmomatic/"

paste 命令将每一列用 'tab' 键分隔，要换成 'space' 键（用 'sed' 命令）

```shell
cat config_trimmomatic | sed 's/\t/ /g' | xargs -i echo nohup trimmomatic PE -phred33 {} ILLUMINACLIP:/home/u3065/anaconda3/envs/rna/share/trimmomatic-0.38-1/adapters/TruSeq3-PE-2.fa:2:30:10 LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 MINLEN:36 \& > trimmomatic.sh

bash trimmomatic.sh
```



---



## 6. 比对

### 6.1. hisat2

#### *创建索引：hisat2 index generation*

当前路径： "~/reference/index/hisat/"

```shell
reference_home='/home/u3065/reference'

hisat2_extract_exons.py $reference_home/gtf/Arabidopsis_thaliana.TAIR10.28.gtf > TAIR10_genome.exon
hisat2_extract_splice_sites.py $reference_home/gtf/Arabidopsis_thaliana.TAIR10.28.gtf > TAIR10_genome.ss
hisat2_extract_snps_haplotypes_VCF.py snp142Common.txt > TAIR10_genome.snp

nohup hisat2-build -p 10 --snp TAIR10_genome.snp --ss TAIR10_genome.ss --exon TAIR10_genome.exon $reference_home/genome/Arabidopsis_thaliana.TAIR10.28.dna.genome.fa genome_snp_tran &
```

##### **注意**：

* ##### `TAIR10_genome.exon` 和 `TAIR10_genome.ss` 为必选， `TAIR10_genome.snp` 可选。

* ##### hisat2构建genome index时，只能用 .gtf文件，而不能用 .gff3文件，最后将genome.fastq和genome.gtf解压！！

```shell
## 利用cufflinks可对注释文件进行格式转化
#gff2gtf
gffread input.gff3 -T -o out.gtf
gffread input.gff -T -o out.gtf
#gtf2gff
gffread input.gtf -o output.gff3
gffread input.gtf -o output.gff3
```



#### *比对：alignment by hisat2* 

当前路径为："04.alignment"

```shell
hisat_TAIR10_index='/home/u3065/reference/index/hisat/TAIR10/TAIR10_genome'
clean_fq='/home/u3065/my_project/03.clean_data/trim_galore'
```

#### *批量比对：high-throughout alignment I by hisat2*

当前路径为："05.fast_alignment"

```shell
hisat_TAIR10_index='/home/u3065/reference/index/hisat/TAIR10/TAIR10_genome'
clean_fq='/home/u3065/my_project/03.clean_data/trim_galore'
for i in $(seq 194 209); do (nohup hisat2 -p 20 -x $hisat_TAIR10_index -1 $clean_fq/ERR1698${i}_1_val_1.fq.gz -2 $clean_fq/ERR1698${i}_2_val_2.fq.gz -S ERR1698${i}.hisat2.sam &); done
```

#### *批量比对：high-throughout alignment II by hisat2*

当前路径为："05.fast_alignment"

创建配置文件：config_clean_data

```shell
ls ../03.clean_data/trim_galore/*_1_val_1.fq.gz | while read id; do (echo $(basename $id "_1_val_1.fq.gz")); done > 1
ls ../03.clean_data/trim_galore/*_1_val_1.fq.gz > 2
ls ../03.clean_data/trim_galore/*_2_val_2.fq.gz > 3
paste 1 2 3 > config_clean_data
```

shell 脚本：

```shell
hisat_TAIR10_index='/home/u3065/reference/index/hisat/TAIR10/TAIR10_genome'
cat config_clean_data | while read id
do 
	arr=($id)
	sample=${arr[0]}
	fq1=${arr[1]}
	fq2=${arr[2]}
nohup hisat2 -p 20 -x $hisat_TAIR10_index -1 $fq1 -2 $fq2 -S ${sample}.hisat2.sam &
done
```



### 6.2. star

#### *创建索引：star index generation*

当前路径：''~/reference/index/star/TAIR10/"

```shell
star_index='/home/u3065/reference/index/star/TAIR10/'
genome=/home/u3065/reference/genome/Arabidopsis_thaliana.TAIR10.28.dna.genome.fa
gtf=/home/u3065/reference/gtf/Arabidopsis_thaliana.TAIR10.28.gtf
nohup STAR --runMode genomeGenerate \
	--genomeDir $star_index \
	--genomeFastaFiles $genome  \
	--sjdbGTFfile $gtf \
	--runThreadN 10 &
```

#### *比对：alignment by star*

当前路径："04.alignment"

```shell
star_index='/home/u3065/reference/index/star/TAIR10/'
clean_fq='/home/u3065/my_project/03.clean_data/trim_galore'
nohup STAR --genomeDir $star_index \
	--readFilesIn $clean_fq/ERR1698194_1_val_1.fq.gz $clean_fq/ERR1698194_2_val_2.fq.gz \
	--readFilesCommand zcat \
	--outFileNamePrefix ERR1698194.star \
	--runThreadN 10 &
```

#### *批量比对：high-throughout alignment by star*

当前路径为："05.fast_alignment"

```shell
star_index='/home/u3065/reference/index/star/TAIR10/'
clean_fq='/home/u3065/my_project/03.clean_data/trim_galore'
for i in $(seq 194 209); do (nohup STAR --genomeDir $star_index --readFilesIn $clean_fq/ERR1698${i}_1_val_1.fq.gz $clean_fq/ERR1698${i}_2_val_2.fq.gz --readFilesCommand zcat --outFileNamePrefix ERR1698${i}.star --runThreadN 10 &); done
```



### 6.3. subread

#### *创建索引：subread index generation*

当前路径：''~/reference/index/star/TAIR10/"

```shell
genome=/home/u3065/reference/genome/Arabidopsis_thaliana.TAIR10.28.dna.genome.fa
nohup subread-buildindex -o TAIR10  $genome &
```

#### *比对：alignment by subjunc*

当前路径："04.alignment"

**subjunc 直接生成 bam 文件**

```shell
subread_index='/home/u3065/reference/index/subread/TAIR10'
clean_fq='/home/u3065/my_project/03.clean_data/trim_galore'
nohup subjunc -T 10 -i $subread_index -r $clean_fq/ERR1698194_1_val_1.fq.gz -R $clean_fq/ERR1698194_2_val_2.fq.gz -o ERR1698194.subjunc.bam &
```

#### *批量比对：high-throughout alignment by subjunc*

当前路径为："05.fast_alignment"

```shell
subread_index='/home/u3065/reference/index/subread/TAIR10'
clean_fq='/home/u3065/my_project/03.clean_data/trim_galore'
for i in $(seq 194 209); do (subjunc -T 10 -i $subread_index -r $clean_fq/ERR1698${i}_1_val_1.fq.gz -R $clean_fq/ERR1698${i}_2_val_2.fq.gz -o ERR1698${i}.subjunc.bam &); done
```



### 6.4. bwa

#### *创建索引：bwa index generation*

当前路径：''~/reference/index/star/TAIR10/"

```shell
genome=/home/u3065/reference/genome/Arabidopsis_thaliana.TAIR10.28.dna.genome.fa
nohup bwa index -a is -p TAIR10_genome $genome &
```

#### *比对：alignment by bwa*

当前路径："04.alignment"

```shell
bwa_index='/home/u3065/reference/index/bwa/TAIR10/TAIR10_genome'
clean_fq='/home/u3065/my_project/03.clean_data/trim_galore'
nohup bwa mem -t 10 $bwa_index $clean_fq/ERR1698194_1_val_1.fq.gz $clean_fq/ERR1698194_2_val_2.fq.gz > ERR1698194.bwa.sam &
```

#### *批量比对：high-throughout alignment by bwa*

当前路径为："05.fast_alignment"

```shell
bwa_index='/home/u3065/reference/index/bwa/TAIR10/TAIR10_genome'
clean_fq='/home/u3065/my_project/03.clean_data/trim_galore'
for i in $(seq 194 209); do (nohup bwa mem -t 10 $bwa_index $clean_fq/ERR1698${i}_1_val_1.fq.gz $clean_fq/ERR1698${i}_2_val_2.fq.gz > ERR1698${i}.bwa.sam &); done
```



### 6.5. bowtie2

#### *创建索引：bowtie2 index generation*

当前路径：''~/reference/index/star/TAIR10/"

```shell
genome=/home/u3065/reference/genome/Arabidopsis_thaliana.TAIR10.28.dna.genome.fa
nohup bowtie2-build $genome TAIR10 &
```

#### *比对：alignment by bowtie2*

当前路径："04.alignment"

```shell
bowtie_index='/home/u3065/reference/index/bowtie/TAIR10'
clean_fq='/home/u3065/my_project/03.clean_data/trim_galore'
nohup bowtie2 -p 10 -x $bowtie_index -1 $clean_fq/ERR1698194_1_val_1.fq.gz -2 $clean_fq/ERR1698194_2_val_2.fq.gz -S ERR1698194.bowtie.sam &
```

#### *批量比对：high-throughout alignment by bowtie2*

当前路径为："05.fast_alignment"

```shell
bowtie_index='/home/u3065/reference/index/bowtie/TAIR10'
clean_fq='/home/u3065/my_project/03.clean_data/trim_galore'
for i in $(seq 194 209); do (nohup bowtie2 -p 10 -x $bowtie_index -1 $clean_fq/ERR1698${i}_1_val_1.fq.gz -2 $clean_fq/ERR1698${i}4_2_val_2.fq.gz -S ERR1698${i}.bowtie.sam &); done
```



---



## 7. sam2bam

来自Jimmy Zeng 的 shell 脚本：

```shell
# 注意：sam2bam内存有可能hold不住，调整-@或者样品分组进行。
ls *.sam | while read id; do (nohup samtools sort -O bam -@ 20 -o $(basename ${id} ".sam" ).bam $id &); done

ls *.bam | xargs -i samtools index {}      # 等价于 ls *.bam | while read id; do (nohup samtools index $id &); done

ls *.bam | while read id; do (nohup samtools flagstat -@ 20 $id > $(basename ${id} ".bam").flagstat &); done  

mkdir flagstat
mv *.flagstat flagstat
multiqc ./
```



samtools 的常用方法：

```shell
# sam2bam
ls *.sam | while read id; do (nohup samtools view -b -S -@ 20 $id > $(basename ${id} '.sam').bam &); done

# bam 文件排序
ls *.bam | while read id; do (nohup samtools sort -@ 20 -o $(basename ${id} '.bam').sorted.bam $id &); done

# bam 文件生成 index
ls *.sorted.bam | while read id; do (nohup samtools index $id &); done

# mapping 结果的评估（执行这一步前需要经过sort和index）
ls *.sorted.bam | while read id; do (nohup samtools idxstats -@ 20 $id > $(basename ${id} '.bam').idxstats &); done

# 统计 bam 文件中的比对flag信息，并输出比对统计结果
ls *.sorted.bam | while read id; do (nohup samtools flagstat -@ 20 $id > $(basename ${id} ".bam").flagstat &); done

# 合并 bam 文件
samtools merge -n out.bam in1.bam in2.bam in3.bam…
```



---



## 8. 表达定量

### 8.1. featureCounts

当前路径："06.count"

```shell
gtf=/home/u3065/reference/gtf/Arabidopsis_thaliana.TAIR10.28.gtf
nohup featureCounts -T 20 -p -t exon -g gene_id -a $gtf -o featureCounts.all.id.txt ../05.fast_alignment/*.bam &

multiqc ./
```



### 8.2. HTseq

当前路径："06.count"

```shell
gtf=/home/u3065/reference/gtf/Arabidopsis_thaliana.TAIR10.28.gtf
nohup htseq-count -f bam -r pos ../../05.fast_alignment/*.bam $gtf 1>htseq.count.txt 2>htseq.count.log &

multiqc ./
```



---



## 9. 差异表达分析

读入数据

当前路径："07.DEG"

```R
## loading count_matrix
if(T){
  rm(list = ls())
  options(stringsAsFactors = F)
  countdata <- read.table("featureCounts_all_counts.txt",
                 sep = "\t",
                 header = T,
                 row.names = 1)
  temp <- a[1:8,1:8]
  meta <- a[,1:5]
  exprSet <- a[,6:ncol(a)]
  rownames(exprSet)
  colnames(exprSet)
  colnames(exprSet) <- c(paste("ERR1698", 194:209, sep = ""))
  
}

## exploring count_matrix
if(F){
  # 样品间相关关系热图1
  library(corrplot)
  png("corrplot.png")
  corrplot(cor(exprSet))
  dev.off()
  
  # 样品间相关关系热图2
  png("corrplot1.png")
  corrplot(cor(log2(exprSet+1)))
  dev.off()
  
  # 样品间相关关系热图3
  library(pheatmap)
  png("heatmap.png")
  m <- cor(log2(exprSet+1))
  pheatmap(m)
  dev.off()
}
```





### 9.1. [DESeq2: differentially expressed analysis](https://github.com/jmzeng1314/my-R/blob/master/10-RNA-seq-3-groups/hisat2_mm10_htseq.R)

当前路径："07.DEG"

```R
## DESeq2 for differential expression analysis
if(T){
  # 安装DESeq2
  #source ("http://bioconductor.org/biocLite.R")
  #biocLite("DESeq2")
  
  # 载入DESeq2
  library(DESeq2)
  
  example <- read.table("example.txt")
  
  # DESeqDataSetFromMatrix参数配置
  group_list <- c('0_h','2_h','2_h','2_h',
                 '3_h','3_h','3_h','3_h',
                 '0_h','0_h','0_h','1_h',
                 '1_h','1_h','1_h','2_h')
  colData <- data.frame(row.names=colnames(exprSet), 
                       group_list=group_list)
  
  # 构建DESeq2的dds对象
  dds <- DESeqDataSetFromMatrix(countData = exprSet,
                               colData = colData,
                               design = ~ group_list)
  
  # 查看dds矩阵的前6行
  head(dds)
  
  # 对dds进行Normalize
  dds <- DESeq(dds)
  
  # 查看结果的名称
  resultsNames(dds)
  
  # 使用results()函数获取结果，并赋值给res
  res <- results(dds,
                contrast=c("group_list","3_h","0_h"),
                alpha = 0.05, pAdjustMethod = "fdr")
  
  # 查看res矩阵的前5行
  head (res, n = 5)
  
  # 对res矩阵进行总结
  summary(res)
  
  # 对res矩阵根据padj进行排序
  resOrdered <- res[order(res$padj),]
  
  # 查看resOrdered矩阵中的前6行
  head(resOrdered)
  
  # 将resOrdered转化为data.frame格式，并赋值给DEG_3vs0_h
  # 提取差异表达结果
  DEG_3vs0_h <- as.data.frame(resOrdered)
  
  # 删除"NA"的行
  DEG_3vs0_h <- na.omit(DEG_3vs0_h)
  
  # Subset for only significant genes (q < 0.05)
  #results_sig <- subset(res, padj < 0.05 & abs(log2FoldChange) > 1)
  results_sig <- subset(res, padj < 0.05)
  head(results_sig)
  
  # Write differentilly expressed gene results to a .txt file
  write.table(x = DEG_3vs0_h,
              file = "DEG_deseq2_results.txt",
              sep = "\t",
              quote = F,
              col.names = NA)
  
  # Write rlog normolized gene expression matrix to a .txt file
  dds_rlog <- rlog(dds, blind = FALSE)
  exprMatrix_rlog <- assay(dds_rlog) 
  write.table(exprMatrix_rlog,
            file = 'normalized_exp_rlog_deseq2.txt',
            sep = '\t', 
            quote = F,
            col.names = NA)
  
  # Write normalized gene counts to a .txt file
  normalized_counts_deseq2 <- as.data.frame(counts(dds), normalized = T)
  write.table(normalized_counts_deseq2, 
              file = 'normalized_counts_deseq2.txt', 
              sep = '\t', 
              quote = F,
              col.names = NA)
  
  # Write significant normalized gene counts to a .txt file
  normalized_counts_significant_deseq2 <- counts(dds[row.names(results_sig)], normalized = T)
  write.table(normalized_counts_significant_deseq2, 
              file = 'normalized_counts_significant_deseq2.txt', 
              sep = '\t', 
              quote = F, 
              col.names = NA)
  
}

## Plotting Gene Expression Data
if(T){
  # Convert all samples to rlog
  dds_rlog <- rlog(dds, blind = FALSE)
  
  ## Plot PCA by column variable
  plotPCA(dds_rlog, intgroup = "group_list", ntop = 500) +
    theme_bw() + # remove default ggplot2 theme
    geom_point(size = 5) + # Increase point size
    scale_y_continuous(limits = c(-10, 10)) + # change limits to fix figure dimensions
    ggtitle(label = "Principal Component Analysis (PCA)", 
            subtitle = "Top 500 most variable genes") 
  
  ## volcano plot
  library(ggplot2)
  #log2FoldChange_cutoff = with(DEG_3vs0_h, mean(abs(log2FoldChange)) + 2*sd(abs(log2FoldChange)))
  log2FoldChange_cutoff <- 1
  
  DEG$change <- as.factor(ifelse(DEG_3vs0_h$padj < 0.05 & abs(DEG_3vs0_h$log2FoldChange) > log2FoldChange_cutoff,
                                ifelse(DEG_3vs0_h$log2FoldChange > log2FoldChange_cutoff ,'UP','DOWN'),'NOT'))
  this_tile <- paste0('Cutoff for logFC is ',round(log2FoldChange_cutoff,3),
                     '\nThe number of up gene is ',nrow(DEG_3vs0_h[DEG_3vs0_h$change =='UP',]) ,
                     '\nThe number of down gene is ',nrow(DEG_3vs0_h[DEG_3vs0_h$change =='DOWN',]))
  
  volcano <- ggplot(data = DEG_3vs0_h, 
                   aes(x = log2FoldChange, y = -log10(padj), 
                       color = change)) +
    geom_point(alpha = 0.5, size = 1.75) +
    theme_set(theme_set(theme_bw(base_size=20)))+
    xlab("log2FoldChange") + ylab("-log10padj") +
    ggtitle(this_tile) + theme(plot.title = element_text(size = 15,hjust = 0.5))+
    scale_colour_manual(values = c('blue','black','red')) ## corresponding to the levels(res$change)
  print(volcano)
  ggsave(volcano,filename = "volcano.pdf")
  
  ## heatmap
  library(pheatmap)
  choose_gene=head(rownames(DEG_3vs0_h),50) # 50 maybe better
  choose_matrix=exprSet[choose_gene,]
  choose_matrix=t(scale(t(choose_matrix)))
  pheatmap(choose_matrix,filename = 'DEG_top100_heatmap.png')
  
  ## Plot Dispersions
  png("qc_dispersions.png", 1000, 1000, pointsize=20)
  plotDispEsts(dds, main="Dispersion plot")
  dev.off()
  
  ## from jmzeng
  png("DEseq_RAWvsNORM.png",height = 800,width = 800)
  par(cex = 0.7)
  n.sample=ncol(exprSet)
  if(n.sample>40) par(cex = 0.5)
  cols <- rainbow(n.sample*1.2)
  par(mfrow=c(2,2))
  boxplot(exprSet, col = cols,main="expression value",las=2)
  boxplot(exprMatrix_rlog, col = cols,main="expression value",las=2)
  hist(as.matrix(exprSet))
  hist(exprMatrix_rlog)
  dev.off()
  
}

## other function usages in DESeq2
if(F){
  # 得到经过DESeq2软件normlization的表达矩阵
  rld <- rlog=(dds)
  exprMatrix_rlog <- assay(rld) 
  
  normalizedCounts1 <- t( t(counts(dds)) / sizeFactors(dds) )
  # normalizedCounts2 <- counts(dds, normalized=T) # it's the same for the tpm value
  # we also can try cpm or rpkm from edgeR package
  exprMatrix_rpm=as.data.frame(normalizedCounts1) 
  head(exprMatrix_rpm)
  write.csv(exprMatrix_rpm,'exprMatrix.rpm.csv' )
  
  library(RColorBrewer)
  (mycols <- brewer.pal(8, "Dark2")[1:length(unique(group_list))])
  cor(as.matrix(exprSet))
  # Sample distance heatmap
  sampleDists <- as.matrix(dist(t(exprMatrix_rlog)))
  #install.packages("gplots",repos = "http://cran.us.r-project.org")
  library(gplots)
  png("qc-heatmap-samples.png", w=1000, h=1000, pointsize=20)
  heatmap.2(as.matrix(sampleDists), key=F, trace="none",
            col=colorpanel(100, "black", "white"),
            ColSideColors=mycols[group_list], RowSideColors=mycols[group_list],
            margin=c(10, 10), main="Sample Distance Matrix")
  dev.off()
  
  cor(exprMatrix_rlog) 
}
```



### 9.1. [edgeR: differentially expressed analysis](https://github.com/jmzeng1314/my-R/blob/master/10-RNA-seq-3-groups/hisat2_mm10_htseq.R)

当前路径："07.DEG"



```R
## edgeR for differential expression analysis
{
  source("http://bioconductor.org/biocLite.R")
  biocLite("edgeR")
  library(edgeR)
  example = read.table("example.txt")
  group_list = c('0h','2h','2h','2h',
                 '3h','3h','3h','3h',
                 '0h','0h','0h','1h',
                 '1h','1h','1h','2h')
  DEG = DGEList(counts=exprSet,group=factor(group_list))
  DEG$samples$lib.size = colSums(DEG$counts)
  #利用库的大小来进行标准化TMM
  DEG = calcNormFactors(DEG)
  DEG$samples
  ## The calcNormFactors function normalizes for RNA composition by finding a set of scaling
  ## factors for the library sizes that minimize the log-fold changes between the samples for most
  ## genes. The default method for computing these scale factors uses a trimmed mean of Mvalues
  ## (TMM) between each pair of samples
  
  png('edgeR_MDS.png')
  plotMDS(DEG, method="bcv", col=as.numeric(DEG$samples$group))
  dev.off()
  
  # The glm approach to multiple groups is similar to the classic approach, 
  # but permits more general comparisons to be made
  
  design = model.matrix(~0+factor(group_list))
  rownames(design) = colnames(DEG)
  colnames(design) = levels(factor(group_list))

  DEG = estimateGLMCommonDisp(DEG,design)
  DEG = estimateGLMTrendedDisp(DEG, design)
  DEG = estimateGLMTagwiseDisp(DEG, design)
  
  library(dplyr)
  design = as.data.frame(design)
  design_3vs0_h = select(design, starts_with("0"), starts_with("3"))
  
  
  fit <- glmFit(DEG, design_3vs0_h)
  lrt <- glmLRT(fit, contrast=c(0,1))
  DEG_3vs0_h=topTags(lrt, n=nrow(exprSet))
  DEG_3vs0_h=as.data.frame(DEG_3vs0_h)
  head(nrDEG)
  write.csv(nrDEG,"DEG_3vs0_h_edgeR.results.csv",quote = F)
}
```



---



## 10. 富集分析

当前目录："08.Enrichment"

```R
if(T){
  rm(list = ls())
  options(stringsAsFactors = F)
  
  # 下载dplyr包
  install.packages("dplyr")
  
  # 载入dplyr包
  library(dplyr)
  
  # 导入差异表达结果
  DEG_3vs0_h <- read.table(file = "DEG_deseq2_results.txt",
                           header = T,
                           sep = "\t",
                           row.names = 1)
  # 筛选差异表达基因（abs(log2FoldChange) > 1 并且 padj < 0.05）
  DEG_3vs0_h_sig <- mutate(DEG_3vs0_h,
                          GeneID = rownames(DEG_3vs0_h)) %>%
    filter(abs(log2FoldChange) > 1, padj < 0.05)
  
  # 差异表达基因列表
  DEG_3vs0_h_list <- as.factor(DEG_3vs0_h_re$GeneID)
  
  # OrgDb 是 Bioconductor 中存储不同数据库基因ID之间对应关系
  # 以及基因与GO等注释的对应关系的 R 软件包。
  # 支持19个物种，可以到这里查询：
  # http://bioconductor.org/packages/release/BiocViews.html#___OrgDb
  #不支持的物种可以通过 AnnotationForge 自己构建。
  
  # 下载org.At.tair.db包
  install.packages("org.At.tair.db")
  
  # 载入org.At.tair.db包
  library(org.At.tair.db)
  
  # 查看keytype
  keytypes(org.At.tair.db)
  
  # 下载clusterProfiler包
  install.packages("clusterProfiler")
  
  # 载入clusterProfiler包
  library(clusterProfiler)
  library(DOSE)
  
  # 进行go富集
  ego <- enrichGO(gene = DEG_3vs0_h_list,
                  keyType = "TAIR",
                  OrgDb = org.At.tair.db,
                  ont = "BP",
                  pAdjustMethod = "fdr",
                  pvalueCutoff = 0.05,
                  qvalueCutoff = 0.05,
                  readable = TRUE)
  
  # 画泡泡图（dotplot）
  dotplot(ego)
  
  # 画条形图（barplot）
  barplot(ego, showCategory=8)
  
  # 保存barplot
  png("go_barplot.png")
  barplot(ego, showCategory=8)
  dev.off()
  
  # 生成数据框
  ego_BP <- as.data.frame(ego)
  
  # 进行kegg富集
  ekp <- enrichKEGG(gene = DEG_3vs0_h_list,
                    organism = 'ath',
                    keyType = "kegg",
                    pvalueCutoff = 0.05,
                    pAdjustMethod = "fdr",
                    qvalueCutoff = 0.05)
  
  # 画条形图（barplot）
  barplot(ekp, showCategory=20, x = "GeneRatio")
  
  # 画泡泡图（dotplot）
  dotplot(ekp)
  
  # 保存dotplot
  png("kegg_dotplot.png")
  dotplot(ekp)
  dev.off()
  
  # 生成数据框
  ekp_results <- as.data.frame(ekp)
  
}
```
